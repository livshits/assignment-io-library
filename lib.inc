section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .string_length_loop:
        cmp byte [rdi + rax], 0
        je .string_length_end
        inc rax
        jmp .string_length_loop

    .string_length_end:    
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov r8, rdi
    call string_length
    mov r9, rax

    mov rdi, 1
    mov rax, 1
    mov rsi, r8
    mov rdx, r9
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1

    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov  rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, 10 ;devider
    xor r9, r9 ;counter
    mov rax, rdi

    .get_last_number:
        xor rdx, rdx
        div r8
        add rdx, 48 ;ascii code
        dec rsp
        mov [rsp], dl
        inc r9

        test rax, rax
        jne .get_last_number

    
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, r9
    syscall

    add rsp, r9
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 1
    xor rcx, rcx

    .check_symbol:
        mov al, [rdi]
        cmp al, [rsi]
        jne .no

        inc rdi
        inc rsi
        test al, al
        jnz .check_symbol
        mov rax, 1
        ret

    .no:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    .start:
        push rsi
        push rdi
        push rdx
        push 0x0
    .read:
        mov rsi, rsp
        mov rdi, 0
        mov rdx, 1
        mov rax, 0
        syscall
    .end:
        pop rax
        pop rdx
        pop rdi
        pop rsi
        ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    push r8
    xor r8, r8
    .ignore_spaces:
        call read_char
        cmp rax, 0x20
        je .ignore_spaces
        cmp rax, 0x9
        je .ignore_spaces
        cmp rax, 0xA
        je .ignore_spaces
        test rax, rax
        jz .success
    .read:
        cmp r8, rsi
        ja .error
        cmp rax, 0x20
        je .success
        cmp rax, 0x9
        je .success
        cmp rax, 0xA
        je .success
        cmp rax, 0
        je .success
        mov [rdi+r8], rax
        inc r8
        call read_char
        jmp .read
    .success:
        mov [rdi+r8+1], byte 0
        mov rax, rdi
        mov rdx, r8
        jmp .end
    .error:
        mov rax, 0
    .end:
        pop r8
        ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rsi, rsi
    xor rdx, rdx
    xor rax, rax

    .parse:
        mov sil, byte[rdi + rdx]
        cmp rsi, '0'
        jb .end
        cmp rsi, '9'
        ja .end
        sub sil, 48
        imul rax, 10
        add al, sil
        inc rdx
        jmp .parse

    .end:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .neg
    call parse_uint
    jmp .end

    .neg:
        inc rdi
        call parse_uint
        cmp rdx, 0
        jz .end
        neg rax
        inc rdx
    .end:
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi rsi rdx
string_copy:
    xor rax, rax

    .loop:
        cmp rax, rdx
        je .buffer_full

        mov cl, [rdi+rax]
        mov [rsi+rax], cl

        cmp cl, 0
        je .ok
        inc rax
        jmp .loop

    .buffer_full:
        xor rax, rax
        
    .ok:
        ret

